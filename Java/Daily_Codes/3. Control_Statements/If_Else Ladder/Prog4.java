

// Tempreature of person.
// > 98.6               => High.

// 98.0 <= and >= 98.6  => Normal.

// < 98.0                => Low.



class Temperature {

	public static void main(String[] args) {

		double temp = 98.5;

		if(temp > 98.6) {

			System.out.println("Temp is High");

		}else if(temp >= 98.0 && temp <= 98.6) {

			System.out.println("Temp is Normal");

		}else{

			System.out.println("Temp is Low");
		}
	}
}


