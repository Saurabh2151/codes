

// Print Integer form 8 to 1 Using While Loop.


class Number {

	public static void main(String[] args) {

		int i = 8;

		while(i >= 1) {

			System.out.println(i);
			i--;
		}
	}
}
