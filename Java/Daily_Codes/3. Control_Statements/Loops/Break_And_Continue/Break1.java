/*
 

    Break using the Without Any Loop.

*/



class Break {

	public static void main(String[] args) {

		int N = 20;

		if(N % 3 == 0) {

			break;

		}
		N--;
	}
}
