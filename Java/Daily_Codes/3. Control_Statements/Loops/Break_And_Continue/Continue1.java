/*
 

   With out loop using Continue.

*/



class Continue {

	public static void main(String[] args) {

		int N = 50;

		if(N % 2 == 0) {

			continue;

		}
		N--;
	}
}
