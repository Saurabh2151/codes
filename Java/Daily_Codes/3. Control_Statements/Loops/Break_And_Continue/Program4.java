/*
 

   Print the 1 to 40 Number is divisible by 3 and 5 or 4 asel tr use break.


*/


class Break {

	public static void main(String[] args) {

		int N = 40;

		for(int i=1;i<=N;i++) {

			if((i % 3 == 0 && i % 5 == 0) || i % 4 == 0) {

				break;

			}
			System.out.println(i);
		}
	}
}
