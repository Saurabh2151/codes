 

/* 
 
   Print Amstrong number in range.

   if the sum of digit raised to count of digit is equal to that number.

*/


class Number  {

	public static void main(String[] args) {

		for(long i = 1;i<999999999999999999l;i++) {

			long num = i;
			long sum = 0;
			long temp = num;
			long temp1 = num;
			long count = 0;

			while(temp != 0) {

				count++;
				temp = temp / 10;
			}
			while(temp1 != 0) {
				
				long rem = temp1 % 10;
				long mult = 1;

				for(int j=0;j<count;j++) {

					mult = mult * rem;

				}
				sum = sum + mult;
				temp1 = temp1 / 10;
			}
			if(num == sum) {

				System.out.println(num);
			}
		}
	}
}


