

class Demo {

	static public void main(string[] args) {

		float f1 = 7.5;   // incompatable type : lossy conversion from double to float.
		float f2 = 7.5;

		System.out.println(f1);
		System.out.println(f2);

	}
}

/*
Solution :

	float f1 = 7.5f;
	float f2 = 7.5f;

	*/
