

class CricPlayer {
	
	int x = 10;             // Instance Variable / Global Variable 
	// Error : Non static variable x.

	public static void main(String[] args) {

		int y = 20;

		System.out.println(x);
		System.out.println(y);
		
	}
}

/*
 
Solution :

 	static int x = 10;

	*/
