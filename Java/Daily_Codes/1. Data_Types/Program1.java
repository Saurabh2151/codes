

class Demo {

	static public void main(String[] args) {

		byte var1 = 10;
		byte var2 = 20;

		System.out.println(var1);
		System.out.println(var2);

		var1 = var1 + var2;           // byte cha addition number ha by default java int mdhe convert hoto.
					      
					      // error: incompatable type
		System.out.println(var1);
		System.out.println(var2);
	}
}

/*
 
Solution :
 	
	System.out,println(var1 + var2);

	*/
