
class parent {

	parent() {

		System.out.println("In Parent Constructor");

	}
}

class child extends parent{

	child() {

		System.out.println("In Child Constructor");

	}
}
class Client {

	public static void main(String[] args) {

		parent obj1 = new parent();
		child obj2 = new child();
	}
}
