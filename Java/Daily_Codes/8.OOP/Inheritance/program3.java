
class parent {

	parent() {
		System.out.println(this);
		System.out.println("In Parent Constructor");	
	}
	void parentProperty() {

		System.out.println("car,flat,gold");

	}
}

class child extends parent{

	child() {
		
		System.out.println(this);
		System.out.println("In child Constructor");
	}
}

class Client {

	public static void main(String[] args) {

		child obj = new child();
		System.out.println(obj);
		obj.parentProperty();
	}
}

