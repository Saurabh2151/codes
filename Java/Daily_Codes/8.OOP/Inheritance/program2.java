class parent {

	parent() {

		System.out.println("In Parent Constructor");

	}
	void parentProperty() {

		System.out.println("Flat,Car,Gold");

	}
}
class child extends parent{

	child() {

		System.out.println("In child Constructor");

	}
}
class Client {

	public static void main(String[] args) {

		child obj = new child();
		obj.parentProperty();
	}
}
