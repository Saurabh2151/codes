
class parent {

	parent() {

		System.out.println("In Parent Constructor");
		System.out.println(this);

	}
	void parentProperty() {

		System.out.println("Car,Gold,Flat");

	}
}
class child extends parent{

	child() {

		System.out.println("In Child Constructor");
		System.out.println(this);

	}
}
class Client {

	public static void main(String[] args) {

		parent obj1 = new parent();

		child obj2 = new child();
		obj2.parentProperty();
	}
}
