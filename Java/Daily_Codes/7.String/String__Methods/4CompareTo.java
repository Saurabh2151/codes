

/*
 
  Method : public int CompareTo(String str2)

  Description : It Compare the str1 and str2 (case Sensetive)
	      if both the string or equal it return 0. str1
	      CompareTo str2 if both string are not equal 
	      otherwise it return Comparision.

  Parameter : String(Second String)

  return Type : Integer

  */

class CompareToDomo {

	public static void main(String[] args) {

		String str1 = "Ashish";
		String str2 = "ashish";

		System.out.println(str1.compareTo(str2));

	}
}
		

