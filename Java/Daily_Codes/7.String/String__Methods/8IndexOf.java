

/*
 
   Method : public int indexOf(int ch,int fromIndex);

   Description : Find the first instance of character in a given String.

   Parameter : Character ( ch to find ) Integer (Index to States Search)

   return type : Integre

   */

class IndexOfDemo {

	public static void main(String[] args) {

		String str1 = "Shashi";

		System.out.println(str1.indexOf('h',0));
		System.out.println(str1.indexOf('h',1));
		System.out.println(str1.indexOf('h',2));

	}
}

