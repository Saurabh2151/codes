

/*
 
  Method : public String SubString(int index);

  Description : Create a SubString of the given String at a Specified index
  		and ending at the end of the given String.

  Parameter : Integer(index of String)

  return Type : String

  */

class SubStringDemo {

	public static void main(String[] args) {

		String str1 = "Core2Web";

		System.out.println(str1.substring(0,7));
		System.out.println(str1.substring(4,5));
		System.out.println(str1.substring(1));
	}
}
