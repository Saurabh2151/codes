/*

 Method : public boolean equalsIgnoreCase(String another String)

 Description : Compare a String to this String ignoring case

 Parameter : String(str2)

 return Type : Boolean

 */

class EqualsIgnoreDemo {

	public static void main(String[] args) {

		String str1 = "Core2Web";

		String str2 = new String("Core2Web");

		System.out.println(str1.equalsIgnoreCase(str2));

	}
}
