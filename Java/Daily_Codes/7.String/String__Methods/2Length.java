/*

 Method : public int length();

 Description : - It return the number of characters containd in given string.

 Parameter : No Parameters

 return Type : Integer


*/


class LengthDemo {

	public static void main(String[] args) {

		String str1 = "Core2Web";

		System.out.println(str1.length());

	}
}
