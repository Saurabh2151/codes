
import java.util.*;
class MyCompareToDemo {
	
	static int mystrcom(String str1,String str2) {
			
		char arr1[] = str1.toCharArray();

		char arr2[] = str2.toCharArray();

		if(arr1.length != arr2.length) 

			return arr1.length - arr2.length;
			

		for(int i=0;i<arr1.length;i++) {

			if(arr1[i] != arr2[i]) 

				return arr1[i] - arr2[i];
				
		}
	return 0;
	}
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the String :");

		String str1 = sc.next();
		String str2 = sc.next();

		int Difference = mystrcom(str1,str2);

		System.out.println("Difference = " +Difference);

	}
}
