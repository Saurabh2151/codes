


class HashDemo {

	public static void main(String[] args) {

		String str1 = "Saurabh";
		String str2 = new String("Vishal");
		String str3 = "Aayush";
		String str4 = new String("Soham");

		System.out.println(str1.hashCode());
		System.out.println(str2.hashCode());
		System.out.println(str3.hashCode());
		System.out.println(str4.hashCode());
	}
}
