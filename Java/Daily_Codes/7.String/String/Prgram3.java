

class StringDemo {

	public static void main(String[] args) {
		
		String str1 = "Rahul";
		String str2 = "Rahul";
		String str3 = new String("Rahul");
		String str4 = new String("Rahul");
		String str5 = new String("Raj");
		String str6 = "Vishal";
		
		System.out.println(System.identityHashCode(str1));
		System.out.println(System.identityHashCode(str2));
		System.out.println(System.identityHashCode(str3));
		System.out.println(System.identityHashCode(str4));
		System.out.println(System.identityHashCode(str5));
		System.out.println(System.identityHashCode(str6));
	}
}


