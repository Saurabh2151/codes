

class StringDemo {

	public static void main(String[] args) {

		String str1 = "Saurabh";
		String str2 = "Ganjale";

		System.out.println(str1 + str2);

		String str3 = str1 + str2;                    // Internally call By append() method in Stringbuilder class.
		String str4 = "SaursbhGanjale";

		System.out.println(System.identityHashCode(str3));
		System.out.println(System.identityHashCode(str4));
	}
}
