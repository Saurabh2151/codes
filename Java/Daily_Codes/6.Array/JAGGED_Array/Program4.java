
import java.io.*;
class ArrayDemo {

	public static void main(String[] args)throws IOException {
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int arr[][] = {{1,2,3},{4,5},{6}};

		int arr1[][] = new int[3][];

		arr1[0] = new int[3];
		arr1[1] = new int[2];
		arr1[2] = new int[1];

		for(int i=0;i<arr1.length;i++) {

			for(int j=0;j<arr1[i].length;j++) {

				arr1[i][j] = Integer.parseInt(br.readLine());

			}
		}

		for(int[] x : arr1) {

			for(int y : x) {

				System.out.print(y+" ");

			}
			System.out.println();
		}
	}
}

