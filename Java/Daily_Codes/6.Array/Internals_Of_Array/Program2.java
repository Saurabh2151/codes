
class Demo {

	public static void main(String[] args) {

		int arr[] = {10,20,30,40};
		float arr1[] = {10.5f,15.5f};
		char arr2[] = {'A','B','C'};
		boolean arr3[] = {true,false,true};
		
		// Integer value
		System.out.println(arr[0]);
		System.out.println(arr[1]);
		System.out.println(arr[2]);
		System.out.println(arr[3]);

		//Float value
		System.out.println(arr1[0]);
		System.out.println(arr1[1]);

		System.out.println(arr1[2]);    // Error : array index out of bound

		// Character value
		System.out.println(arr2[0]);
		System.out.println(arr2[1]);
		System.out.println(arr2[2]);

		
		// Boolean value
		System.out.println(arr3[0]);
		System.out.println(arr3[1]);
		System.out.println(arr3[2]);

	}
}
