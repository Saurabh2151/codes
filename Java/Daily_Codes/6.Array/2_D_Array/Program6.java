
import java.io.*;
class ArrayDemo {

	public static void main(String[] args)throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Rows :");

		int rows = Integer.parseInt(br.readLine());

		System.out.println("Enter the Cols :");

		int cols = Integer.parseInt(br.readLine());

		System.out.println("Enter the Element :");

		int arr[][] = new int[rows][cols];

		for(int i=0;i<arr.length;i++) {

			for(int j=0;j<arr[i].length;j++) {

				arr[i][j] = Integer.parseInt(br.readLine());

			}
		}

		for(int[] x : arr) {

			for(int y : x) {

				System.out.print(y +" ");

			}
			System.out.println();
		}
	}
}

