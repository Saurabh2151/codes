/*
 

   WAP to print the strong Number.

   Strong Number ==> the number whose sum of all digit factorial is equal to the number 'N'.


*/


class Number {

	public static void main(String[] args) {

		int N = 145;
		int temp = N;
		int sum = 0;
		int num = 0;

		while(N != 0) {

			num = N % 10;
			int mult = 1;
			for(int i=1;i<=num;i++) {

				mult = mult * i;

			}
			sum = sum + mult;
			N = N / 10;
		}
		if(temp == sum) {

			System.out.println(temp+ " is a strong Number");

		}else{ 

			System.out.println(temp+ " is not a strong Number");

		}
	}
}


					
