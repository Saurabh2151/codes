class Core2Web {

	public static void main(String[]args) {

		short[][][] arrShort = new short[][][] { {{},{},{}} , {{},{},{}} };

		System.out.println(arrShort.length);
		System.out.println(arrShort[0].length);
		System.out.println(arrShort[0][0].length);
	}
}
/*
Explation : arrShort.length refers to number of 2D arrays in the array i.e. 2 
	    arrShort[0].length refers to number of rows in 0th 2D array i.e. 3 
	    arrShort[0][0].length refers to no. of elements in 0th row of 0th 2D array i.e. 0 

*/
