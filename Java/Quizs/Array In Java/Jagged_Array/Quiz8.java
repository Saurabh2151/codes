class Core2Web{
	public static void main(String[] args) {
		int[] arr1[], arr2;
		arr1 = new int[10];
		arr2 = new int[5];
	}
}

/*
 
Explation :  Here we are declaring an array of type int[][]. 
	     Then we assign it a reference of type int[] so the compiler will flag this as an error.
	     arr1 = new int[10];
	     incompatible types: int[] cannot be converted to int[][]
*/
