class Core2Web {

	public static void main(String[]args) {

		byte[][][] arrByte = new byte[14][30][21];

		System.out.println(arrByte.length);
		System.out.println(arrByte[0].length);
		System.out.println(arrByte[0][0].length);
	}
}

/*
Explanation : arrByte.length refers to number of 2D arrays in the array arrByte[0].length refers to number of rows in 0th 2D array
	      arrByte[0][0].length refers to no. of elements in 0th row of 0th 2D array.

*/
