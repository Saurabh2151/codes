class Core2Web {

	public static void main(String[] args) {
		
		int[][] var1 = new int[2][2] {1,2,3,4};
		System.out.println(var1.length);
	}
}

/*
Explanation : Compile time error(Invalid Declaration) provision of 
	      dimension and initialization both do not happen during creation.

*/
