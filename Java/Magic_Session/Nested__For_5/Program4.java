/*
 
input : Start = 2
	End   = 9

Output : 8  6  4  2
	 3  5  7  9

*/



import java.io.*;
class Demo {

	public static void main(String[] args)throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Start:");

		int Start = Integer.parseInt(br.readLine());

		System.out.println("Enter the End:");
		int End  = Integer.parseInt(br.readLine());

		for(int i=End;i>=Start;i--) {

			if(i % 2 == 0) 

				System.out.print(i+" ");
		}
		System.out.println();
		for(int j=Start;j<=End;j++) {

			if(j % 2 == 1) 

				System.out.print(j+" ");
		}
		System.out.println();
	}
}

			
