


// WAP to calculate Factorial of the given number.


class Factorial {

	public static void main(String[] args) {

		int fact = 1;
		int num = 6;

		while(num > 0) {

			fact = fact * num;

			System.out.println(fact);

			num--;
		}
	}
}
