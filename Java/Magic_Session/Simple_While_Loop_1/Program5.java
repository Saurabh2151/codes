

// WAP to print the square of even digit of the given number.


class Square {

	public static void main(String[] args) {

		int num = 234567891;


		while(num != 0) {

			int i = num % 10;

			if(i % 2 == 0) {

				System.out.println(i * i);
			}
			num = num / 10;
		}
	}
}
