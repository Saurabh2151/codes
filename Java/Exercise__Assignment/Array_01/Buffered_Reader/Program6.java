

/*

   WAP to take the size of array from user and also take integer elements from user print Product of Odd Index only.


   */

import java.io.*;
class ArrayDemo {

	public static void main(String[] args)throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Array Size :");

		int size = Integer.parseInt(br.readLine());

		System.out.println("Enter the Element :");

		int arr[] = new int[size];

		int Index = 0;
		int product = 1;

		for(int i=0;i<size;i++) {
			
			arr[i] = Integer.parseInt(br.readLine());

			if(Index % 2 == 1) {

				product = product * arr[i];

			}
			Index++;
		}
		System.out.println("Odd Index Product ="+product);
	}
}
