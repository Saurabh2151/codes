
/*
 
   WAP take 10 input from the user and print only element that are divisible by 5.

  */

import java.io.*;
class ArrayDemo {

	public static void main(String[] args)throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Array Size :");

		int size = Integer.parseInt(br.readLine());

		System.out.println("Enter the Element :");

		int arr[] = new int[size];

		for(int i=0;i<arr.length;i++) {

			arr[i] = Integer.parseInt(br.readLine());

		}

		for(int j=0;j<arr.length;j++) {

			int num = arr[j];

			if(num % 5 == 0) {

				System.out.print(num+" ");

			}
		}
		System.out.println();
	}
}
