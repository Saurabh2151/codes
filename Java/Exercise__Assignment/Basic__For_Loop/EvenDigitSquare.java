/*
 

   WAP to print the square of even digit of the given number.


*/


class Number {


	public static void main(String[] args) {

		int num = 942111423;
		int N = num;

		for(int i=1;num != 0;i++) {

			N = num % 10;

			if(N % 2 == 0) {

				System.out.println(N * N);

			}
			num = num / 10;
		}
	}
}

