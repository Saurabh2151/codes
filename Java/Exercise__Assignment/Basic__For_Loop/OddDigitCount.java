/*
 

   WAP to print the count of odd digit of the given number.


*/


class Number {

	public static void main(String[] args) {

		int num = 942111423;
		int count = 0;
		int N = 0;

		for(int i=1;num != 0;i++) {
			
			N = num % 10;

			if(N % 2 == 1) {

				count++;

			}
			num = num / 10;
		}
		System.out.println(count);
	}
}



