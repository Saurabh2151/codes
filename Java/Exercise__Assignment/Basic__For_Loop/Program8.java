/*
 
   WAP to print the countdown of days to submit the assignment.
 

*/


class Number {

	public static void main(String[] args) {

		int day = 7;

		for(;day >= 0;day--) {

			if(day >= 1) {

				System.out.println(day+" days are remaining");

			}else{

				System.out.println(day+" days Assignment is Overdue");

			}
		}
	}
}
