
/*

   WAP to find the number of even and odd integer in a given array of integer.


   */


import java.io.*;

class ArrayDemo {

	public static void main(String[] args)throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Array Size :");

		int size = Integer.parseInt(br.readLine());

		System.out.println("Enter the Element :");

		int arr[] = new int[size];

		int count = 0;
		int count1 = 0;

		for(int i=0;i<size;i++) {

			arr[i] = Integer.parseInt(br.readLine());

			if(arr[i] % 2 == 1) {

				count++;

			}else{

				count1++;
			}
		}
		System.out.println("Odd Count ="+count);
		System.out.println("Even Count="+count1);
	}
}


