
/*
 
   WAP to create an Array of 'n' integer number element.
   where 'n' value should be taken from user.
   insert the value from user and find the sum of all element in the array.

   */

import java.io.*;

class ArrayDemo {

	public static void main(String[] args)throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Array Size :");

		int size = Integer.parseInt(br.readLine());

		System.out.println("Enter the Element :");

		int arr[] = new int[size];

		int sum = 0;

		for(int i=0;i<size;i++) {

			arr[i] = Integer.parseInt(br.readLine());

			sum = sum + arr[i];

		}
		System.out.println("Array Sum ="+sum);
	}
}

