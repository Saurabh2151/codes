
/*
  
   WAP to find the common element between two array.

   */

import java.io.*;

class ArrayDemo {

	public static void main(String[] args)throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the First Array Size :");

		int size1 = Integer.parseInt(br.readLine());

		System.out.println("Enter the Element :");

		int arr1[] = new int[size1];

		 for(int i=0;i<arr1.length;i++) {

                        arr1[i] = Integer.parseInt(br.readLine());

                }

		System.out.println("Enter the Second Array Size :");

		int size2 = Integer.parseInt(br.readLine());

		System.out.println("Enter the Element :");

		int arr2[] = new int[size2];

		for(int j=0;j<arr2.length;j++) {

			arr2[j] = Integer.parseInt(br.readLine());

		}

		for(int i=0;i<arr1.length;i++) {

			for(int j=0;j<arr2.length;j++) {

				if(arr1[i] == arr2[j]) {

					System.out.println("Common Element = "+arr1[i]);

				}
			}
		}
	}
}
