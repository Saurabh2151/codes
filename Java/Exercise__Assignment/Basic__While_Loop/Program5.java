/*
 
    WAP to print the square of the even digit of the given number.


*/


class Number {

	public static void main(String[] args) {

		int num = 546842182;
		int square = 0;
		int num1 = 0;

		while(num != 0) {

			num1 = num % 10;

			if(num1 % 2 == 0) {

				square = num1 * num1;
				System.out.println(square);

			}
			num = num / 10;
		}
	}
}
