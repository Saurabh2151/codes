/*
 

   WAP to Calculate the Factorial of the given number.

 
*/


class Number {

	public static void main(String[] args) {

		int num = 6;
		int i = 1;
		int fact = 1;

		while(i <= num) {
			
			fact = fact * i;	
			i++;
		}
		System.out.println(fact);
	}
}


