/*
 
   WAP to print the count of the odd digit of the given number.


*/


class Number {

	public static void main(String[] args) {

		int num = 987524311;
		int count = 0;
		int num1 = 0;

		while(num != 0) {
			num1 = num % 10;

			if(num1 % 2 == 1) {

				count++;

			}
			num = num / 10;
		}
		System.out.println(count);
	}
}
