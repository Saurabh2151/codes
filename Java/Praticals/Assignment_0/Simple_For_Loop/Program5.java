/*
 
   WAP to print the odd Number 1 -50.

   */

class Number {

	public static void main(String[] args) {

		int N = 50;

		for(int i=1;i<=N;i++) {

			if(i % 2 == 1) {

				System.out.println(i);
			}
		}
	}
}

