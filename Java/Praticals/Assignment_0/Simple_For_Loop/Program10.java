/*
 
   WAP to print the Product of the first 10 Number.

   */


class Number {

	public static void main(String[] args) {

		int N = 10;
		int mult = 1;

		for(int i=1;i<=N;i++) {

			mult = mult * i;
		}
		System.out.println(mult);
	}
}
