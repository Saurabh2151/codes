/*

1	2	3	4	
a	b	c	d	
5	6	7	8	
e	f	g	h

*/


#include<stdio.h>

void main() {

	int rows;
	printf("enter the no of rows:\n");
	scanf("%d",&rows);
	
	char ch=97;
	int num=1;
	for(int i=1;i<=rows;i++) {
		for(int j=1;j<=rows;j++) {
			if(i%2==1) {
				printf("%d\t",num);
				num++;
			}else{
				printf("%c\t",ch);
				ch++;
			}
		}
		printf("\n");
	}
}

