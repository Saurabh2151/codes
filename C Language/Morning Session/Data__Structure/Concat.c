

// concat N number string.


#include<stdio.h>
#include<stdlib.h>
typedef struct Node {

	int data;
	struct Node *next;
}node;

node *head1 = NULL;
node *head2 = NULL;

node* createNode() {
	
	node *newnode = (node*)malloc(sizeof(node));

	printf("Enter the data\n");
	scanf("%d",&newnode->data);
	
	newnode->next=NULL;

	return newnode;
	
}

void addnode(node **head) {
	
	node *newnode = createNode();

	if(*head == NULL) {

		*head = newnode;

	}else{

		node *temp = head1;

		while(temp->next != NULL) {

			temp=temp->next;
		}
		temp->next = newnode;
	}
}

void ConcatString(){
	
	node *temp = head1;

	while(temp->next != NULL) {

		temp = temp->next;
	}
	temp->next = head2;
}

void printll(node *head1) {

	node *temp = head1;

	while(temp->next != NULL) {
		
		printf("|%d|-->",temp->data);
		temp=temp->next;
	}
	printf("|%d|",temp->data);

}
void main() {

	int nodes;
	printf("Enter the no of 1 LinkList nodes\n");
	scanf("%d",&nodes);

	for(int i=1;i<=nodes;i++) 

		addnode(&head1);


	printf("Enter the no of 2 LinkList nodes\n");
	scanf("%d",&nodes);

	for(int i=1;i<=nodes;i++) 

		addnode(&head2);

	printll(head1);
	printll(head2);
}
