

#include<stdio.h>
#include<stdlib.h>
typedef struct Node {

	int data;
	struct Node *next;
}node;

node *head = NULL;

node *CreateNode() {

	node *newnode = (node*)malloc(sizeof(node));

	printf("Enter the Data\n");
	scanf("%d",&newnode->data);

	newnode->next = NULL;
	return newnode;
}

int AddNode() {

	node *newnode = CreateNode();

	if(head == NULL) {

		head = newnode;
		newnode->next = head;

	}else{

		node *temp = head;

		while(temp->next != head) {

			temp = temp->next;

		}
		temp->next = newnode;
		newnode->next = head;
	}
	return 0;
}

int DeleteFirst() {

	if(head == NULL) {

		printf("LL Is Empty Boss\n");
		return -1;

	}else if(head->next == head) {

		free(head);
		head = NULL;

	}else{

		node *temp = head;
		
		while(temp->next != head) {

			temp = temp->next;

		}
		head = head ->next;
		free(temp->next);
		temp->next = head;
		return 0;
	}
}

void PrintLL() {

	if(head == NULL) {

		printf("LL is Empty\n");

	}else{

		node *temp = head;

		while(temp->next != head) {

			printf("|%d|-->",temp->data);
			temp = temp->next;
		}
		printf("|%d|",temp->data);
	}
}

void main() {

        char Choice;

        do {

                printf("1.AddNode\n");
		printf("2.DeleteFirst\n");
		printf("3.PrintLL\n");

                int Num;
                printf("Enter your Choice\n");
                scanf("%d",&Num);

                switch(Num) {

                        case 1 :

                                {
                                int Nodes;
                                printf("Enter the no of nodes:\n");
                                scanf("%d",&Nodes);

                                        for(int i=1;i<=Nodes;i++) {

                                                AddNode();
                                        }
                                }
                                break;

                        case 2 :

                                DeleteFirst();
                                break;

			case 3 :

                                PrintLL();
                                break;

                        default :

                                printf("Invalid Choice\n");
                }
                getchar();

                printf("\nDo You Want To Continue\n");
                scanf("%c",&Choice);

        }while(Choice == 'Y' || Choice == 'y');
}

