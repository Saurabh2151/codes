
#include<stdio.h>
#include<stdlib.h>
typedef struct Node {

	int data;
	struct Node *next;

}node;

node *head = NULL;

node *createNode() {

	node *newnode = (node*)malloc(sizeof(node));

	printf("Enter the Data\n");
	scanf("%d",&newnode->data);

	newnode->next = NULL;
	return newnode;

}

int AddNode() {
	
	node *newnode = createNode();

	if(head == NULL) {

		head = newnode;

	}else{

		node *temp = head;

		while(temp->next != NULL) {

			temp = temp->next;
		}
		temp->next = newnode;
	}
	return 0;
}

int AddFirst() {

	node *newnode = createNode();

	if(head == NULL) {

		head = newnode;

	}else{

		newnode->next = head;
		head = newnode;
	}
	return 0;
}

void AddLast() {

	AddNode();
}

int CountNode() {

	node *temp = head;

	int Count=0;
	while(temp->next != NULL) {

		Count++;
		temp = temp->next;
	}
	Count++;
	printf(" NodeCount is = %d\n ",Count);
	return Count;
}

int AddAtPos(int Pos) {


	if(head == NULL) {

		printf("LL is Empty\n");

	}else{

		int Count = CountNode();

		if(Pos <= 0 || Pos > Count+1) {

			printf("Invalid Position\n");
			return -1;
	
		}else if(Pos == 1) {
	
			AddFirst();
			
		}else if(Pos == Count+1) {
	
			AddLast();

		}else{
			
			node *newnode = createNode();

			node *temp = head;

			while(Pos -2) {

				Pos--;
				temp = temp->next;
			}
			newnode->next = temp->next;
			temp->next = newnode;
		}return 0;
	}
}

void PrintLL() {
	
	if(head == NULL) {

		printf("LL is Empty\n");

	}else{

		node *temp = head;

		while(temp->next != NULL) {

			printf("|%d|-->",temp->data);
			temp = temp->next;
		}
		printf("|%d|",temp->data);
	}
}

void main() {

        char Choice;

        do {

                printf("1.AddNode\n");
		printf("2.CountNode\n");
                printf("3.AddFirst\n");
                printf("4.AddLast\n");
		printf("5.AddAtPos\n");
		printf("6.PrintLL\n");

                int Num;
                printf("Enter your Choice\n");
                scanf("%d",&Num);

                switch(Num) {

                        case 1 :

                                {
                                int Nodes;
                                printf("Enter the nof nodes:\n");
                                scanf("%d",&Nodes);

                                        for(int i=1;i<=Nodes;i++) {

                                                AddNode();
                                        }
                                }
                                break;

                        case 2 :

                                CountNode();
                                break;

			case 3 :

				AddFirst();
				break;

			case 4 :

				AddLast();
				break;

			case 5 :

				{
				int Pos;
				printf("Enter the Node Position\n");
				scanf("%d",&Pos);

				AddAtPos(Pos);
				}
				break;

                        case 6 :

                                PrintLL();
                                break;

                        default :

                                printf("Invalid Choice\n");
                }
                getchar();

                printf("\nDo You Want To Continue\n");
                scanf("%c",&Choice);

        }while(Choice == 'Y' || Choice == 'y');
}

