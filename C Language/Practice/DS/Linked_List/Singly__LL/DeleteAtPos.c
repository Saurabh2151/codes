



#include<stdio.h>
#include<stdlib.h>
typedef struct Node {

	int data;
	struct Node *next;
}node;

node *head = NULL;

node *createNode() {

	node *newnode = (node*)malloc(sizeof(node));

	printf("Enter the Data\n");
	scanf("%d",&newnode->data);

	newnode->next = NULL;
	return newnode;

}

int AddNode() {

	node *newnode = createNode();

	if(head == NULL) {

		head = newnode;

	}else{

		node *temp = head;

		while(temp->next != NULL) {

			temp = temp->next;
		}
		temp->next = newnode;
	}
	return 0;
}

int DeleteFirst() {

	if(head == NULL) {

		printf("LL is Empty\n");
		return -1;

	}else{

		node *temp = head;

		head =  head->next;
		free(temp);
	}
	return 0;
}

int DeleteLast() {

	if(head == NULL) {

		printf("LL is Empty\n");
		return -1;

	}else{

		if(head->next == NULL) {

			free(head);
			head = NULL;

		}else{
			node *temp = head;

			while(temp->next->next != NULL) {

				temp = temp->next;
			}
			free(temp->next);
			temp->next = NULL;
		}
		return 0;
	}
}

int CountNode() {

	node *temp = head;

	int Count=0;
	while(temp->next != NULL) {

		Count++;
		temp = temp->next;
	}
	Count++;
	printf(" NodeCount is = %d\n ",Count);
	return Count;
}

int DeleteAtPos(int Pos) {

	if(head == NULL) {

		printf("LL is Empty Boss\n");

	}else{

		int Count = CountNode();

		if(Pos <= 0 || Pos > Count) {

			printf("Invalid Choice\n");
			return -1;

		}else if(Pos == 1) {

			DeleteFirst();

		}else if(Pos == Count) {

			DeleteLast();

		}else{

			node *temp = head;

			while(Pos -2 ) {

				Pos--;
				temp = temp->next;
			}
			node *temp1 = temp;

			temp1 = temp->next;
			temp->next = temp1->next;
			free(temp1);
		}
		return 0;
	}
}

void PrintLL() {

	if(head == NULL) {

		printf("LL is Empty\n");

	}else{

		node *temp = head;

		while(temp->next != NULL) {

			printf("|%d|-->",temp->data);
			temp = temp->next;
		}
		printf("|%d|",temp->data);
	}
}

void main() {

        char Choice;

        do {

                printf("1.AddNode\n");
		printf("2.DeleteFirst\n");
                printf("3.DeleteLast\n");
                printf("4.DeleteAtPos\n");
		printf("5.PrintLL\n");
                printf("6.CountNode\n");


                int Num;
                printf("Enter your Choice\n");
                scanf("%d",&Num);

                switch(Num) {

                        case 1 :

                                {
                                int Nodes;
                                printf("Enter the nof nodes:\n");
                                scanf("%d",&Nodes);

                                        for(int i=1;i<=Nodes;i++) {

                                                AddNode();
                                        }
                                }
                                break;

                        case 2 :

                                DeleteFirst();
                                break;

			case 3 :

				DeleteLast();
				break;

			case 4 :
				{
				int Pos;
				printf("Enter the Position\n");
				scanf("%d",&Pos);

				DeleteAtPos(Pos);
				}
				break;

			case 5 :

                                PrintLL();
                                break;

			case 6 :

				CountNode();
				break;

                        default :

                                printf("Invalid Choice\n");
                }
                getchar();

                printf("\nDo You Want To Continue\n");
                scanf("%c",&Choice);

        }while(Choice == 'Y' || Choice == 'y');
}

