


#include<stdio.h>
#include<stdlib.h>

typedef struct Node {

	int data;
	struct Node *next;
}node;

node *head = NULL;

node* createNode() {

	node *newnode = (node*)malloc(sizeof(node));

	printf("Enter the Data:\n");
	scanf("%d",&newnode->data);

	newnode->next = NULL;
	return newnode;
}

int AddNode() {

	node *newnode = createNode();

	if(head == NULL) {

		head =newnode;

	}else{

		node *temp = head;

		while(temp->next != NULL) {

			temp = temp->next;
		}
		temp->next = newnode;
	}
	return 0;
}

void PrintLL() {

	if(head == NULL) {

		printf("List Is Empty\n");

	}else{

		node *temp = head;
		while(temp->next != NULL) {

			printf("|%d|-->",temp->data);
			temp = temp->next;
		}
		printf("|%d|",temp->data);
	}
}

void main() {

	char Choice;

	do {

		printf("1.addnode\n");
		printf("2.printll\n");

		int Num;
                printf("Enter your Choice\n");
                scanf("%d",&Num);

		switch(Num) {

			case 1 :

				{
				int Nodes;
				printf("Enter the nof nodes:\n");
				scanf("%d",&Nodes);

					for(int i=1;i<=Nodes;i++) {

						AddNode();
					}
				}
				break;

			case 2 :

				PrintLL();
				break;

			default :

				printf("Invalid Choice\n");
		}
		getchar();

		printf("\nDo You Want To Continue\n");
		scanf("%c",&Choice);

	}while(Choice == 'Y' || Choice == 'y');
}
	

