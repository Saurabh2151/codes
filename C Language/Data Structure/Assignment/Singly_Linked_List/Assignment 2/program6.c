
#include<stdio.h>
#include<stdlib.h>

typedef struct Node {

	char str[20];
	struct Node *next;
}node;

node *head = NULL;

node* createNode() {

	node *newnode = (node*)malloc(sizeof(node));
	
	getchar();

	printf("Enter the string\n");
	scanf("%[^\n]",newnode->str);
	
	newnode->next = NULL;

	return newnode;
}

void addnode() {

	node *newnode = createNode();
	
	if(head == NULL) {

		head = newnode;
	}else{

		node *temp =head;
		while(temp->next != NULL) {

			temp=temp->next;
		}
		temp->next=newnode;
	}
}

void printstr(int num) {

	node *temp = head;

	while(temp != NULL) {

		int count=0;
		char *ptr = temp->str;
		while(*ptr != '\0') {

			count++;
			*ptr++;
		}
		if(count == num) {
			printf("%s\n",temp->str);
		}
		temp=temp->next;
	}
}

void printll() {
	
	node *temp = head;

	while(temp->next != NULL) {
		
		printf("|%s|-->",temp->str);
		temp=temp->next;
	}
	printf("|%s|",temp->str);
}
void main() {

	char choice;
		
	do {
		printf("1.addnode\n");
		printf("2.printll\n");
		printf("3.printstr\n");

		int ch;
		printf("Enter the choice\n");
		scanf("%d",&ch);

		switch(ch) {

			case 1 :
				addnode();
				break;

			case 2 :
				printll();
				break;

			case 3 :{

				int num;
				printf("Enter the lenght\n");
				scanf("%d",&num);
				printstr(num);
				break;
				}

			default :

				printf("Wrong choice\n");
		}
		getchar();
		printf("\nDo you want to continue\n");
		scanf("%c",&choice);
	}
	while(choice == 'Y' || choice == 'y');
}

