
// WAP to print sum of data element digit.

#include<stdio.h>
#include<stdlib.h>

typedef struct Node {

	int data;
	struct Node *next;
}node;

node *head = NULL;

node* createNode() {

	node *newnode = (node*)malloc(sizeof(node));

	printf("Enter the data\n");
	scanf("%d",&newnode->data);

	newnode->next=NULL;

	return newnode;
}

void addnode() {

	node *newnode = createNode();

	if(head == NULL) {

		head = newnode;
	}else{

		node *temp = head;
		while(temp->next != NULL) {
			temp=temp->next;
		}
		temp->next = newnode;
	}
}

void Sumdigit() {

	node *temp = head;

	while(temp != NULL) {

		int sum=0;
		while(temp->data != 0) {

			sum = sum+(temp->data % 10);

			temp->data = temp->data / 10;
		}
		temp->data = sum;
		temp=temp->next;
	}
}

void printll() {

	node *temp = head;
	while(temp->next != NULL) {

		printf("|%d|-->",temp->data);
		temp=temp->next;
	}
	printf("|%d|\n",temp->data);
}

void main() {

	int nodes;
	printf("Enter the no of nodes\n");
	scanf("%d",&nodes);

	for(int i=1;i<=nodes;i++) {

		addnode();
	}
	printll();
	Sumdigit();
	printll();
}
