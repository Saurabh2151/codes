

#include<stdio.h>
#include<stdlib.h>

typedef struct Node {

	int data;
	struct Node *next;
}node;

node *head=NULL;

node* createNode() {

	node *newnode =(node*)malloc(sizeof(node));

	printf("Enter the data:\n");
	scanf("%d",&newnode->data);

	newnode->next = NULL;

	return newnode;
}

void addnode() {

	node *newnode = createNode();

	if(head == NULL) {
		head = newnode;
	}else{

		node *temp = head;
		while(temp->next != NULL) {

			temp=temp->next;
		}
		temp->next = newnode;
	}
}

void SecLastOcc(int Data) {

	node *temp=head;
	
	int position =0;
	int count =0;
	int SL=0;
	int L=0;

	while(temp != NULL) {

		count++;
		if(Data == temp->data) {
			
			position++;
			SL = L;
			L = count;
		}
		temp=temp->next;
	}
	if(position == 0) {

		printf("Number is not present\n");

	}else if(position == 1) {

		printf("Number is present only once = %d\n",position);
	}else{

		printf("Second Last Occurences is = %d\n",SL);
	}
}

void printll() {
	
	node *temp = head;
	while(temp->next != NULL) {

		printf("|%d|-->",temp->data);
		temp=temp->next;
	}
	printf("|%d|",temp->data);
}
void main() {

	int nodes;
	printf("Enter the nodes:\n");
	scanf("%d",&nodes);

	for(int i=1;i<=nodes;i++) {
		
		addnode();
	}
	printll();

	int Data;
	printf("\nEnter the Element Data\n");
	scanf("%d",&Data);

	SecLastOcc(Data);
}
