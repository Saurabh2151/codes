

#include<stdio.h>
#include<stdlib.h>

typedef struct Node {

	int data;
	struct Node *next;
}node;

node *head = NULL;

node* createNode() {

	node *newnode = (node*)malloc(sizeof(node));

	printf("Enter the data\n");
	scanf("%d",&newnode->data);

	newnode->next=NULL;

	return newnode;
}

void addnode() {

	node *newnode = createNode();

	if(head == NULL) {

		head = newnode;
	}else{

		node *temp = head;
		while(temp->next != NULL) {
			temp=temp->next;
		}
		temp->next = newnode;
	}
}

void SearchOcc(int Data) {

	node *temp = head;

	int count=0;
	while(temp != NULL) {
		if(Data == temp->data) {
			count++;
		}
		temp=temp->next;
	}
	printf("Number is present in %d|--> times\n",count);
}

void printll() {

	node *temp = head;
	while(temp->next != NULL) {

		printf("|%d|-->",temp->data);
		temp=temp->next;
	}
	printf("|%d|",temp->data);
}

void main() {

	int nodes;
	printf("Enter the no of nodes\n");
	scanf("%d",&nodes);

	for(int i=1;i<=nodes;i++) {

		addnode();
	}
	printll();

	int Data;
	printf("\nEnter the Element Data\n");
	scanf("%d",&Data);
	SearchOcc(Data);
}
		
