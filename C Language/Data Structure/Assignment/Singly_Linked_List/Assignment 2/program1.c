

#include<stdio.h>
#include<stdlib.h>

typedef struct Node {

	int data;
	struct Node *next;
}node;

node *head = NULL;

node* createNode() {

	node *newnode=(node*)malloc(sizeof(node));

	printf("Enter the Data:\n");
	scanf("%d",&newnode->data);

	newnode->next=NULL;
	return newnode;
}

void addnode() {

	node *newnode=createNode();

	if(head == NULL) {

		head = newnode;
	}else{
		node *temp = head;
		while(temp->next != NULL) {

			temp=temp->next;
		}
		temp->next=newnode;
	}
}

int firstocc(int Data) {

	node *temp = head;

	int index=0;
	int count=0;
	while(temp != NULL) {

		index++;
		if(Data == temp->data) {
			count++;	
			break;
		}
		temp= temp->next;
	}
	if(count == 0) {
		printf("Number is not found\n");
	}else{
		printf("position = %d\n",index);
	}
}

void printll() {

	node *temp = head;
	while(temp->next != NULL) {

		printf("|%d|-->",temp->data);
		temp=temp->next;
	}
	printf("|%d|\n",temp->data);
}

void main() {

	int nodes;
	printf("Enter the no of nodes:\n");
	scanf("%d",&nodes);

	for(int i=1;i<=nodes;i++) {

		addnode();
	}
	printll();
	
	int Data;
	printf("Enter the Elements:\n");
	scanf("%d",&Data);

	firstocc(Data);
}
