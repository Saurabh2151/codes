/*

  WAP for the linked list of states in india consisting of its name,population,budget
  connect 4 state in the linkedlist & print their data.

*/



#include<stdio.h>
#include<stdlib.h>
typedef struct state {

	char Sname[20];
	long population;
	float Budget;
	struct state *next;
}ste;

ste *head=NULL;

void addnode() {

	ste *newnode=(ste*)malloc(sizeof(ste));

	printf("enter the State:\n");
	fgets(newnode->Sname,15,stdin);

	printf("Enter the population of state:\n");
	scanf("%ld",&newnode->population);

	printf("Enter the budget in state:\n");
	scanf("%f",&newnode->Budget);

	getchar();

	newnode->next=NULL;

	if(head==NULL) {
		head=newnode;
	}else{
		ste *temp=head;
		while(temp->next!=NULL) {

			temp=temp->next;
		}
		temp->next=newnode;
	}
}

void printll() {

	ste *temp=head;
	while(temp!=NULL) {

		printf("%s",temp->Sname);
		printf("%ld\n",temp->population);
		printf("%f\n",temp->Budget);
		temp=temp->next;
	}
}

void main() {

	addnode();
	addnode();
	addnode();
	addnode();
	printll();
}




