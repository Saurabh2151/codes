/*

5]  write a demo structure consisting of integer data
    Take the no of nodes from the user & print the addition of the integer data.

*/



#include<stdio.h>
#include<stdlib.h>

typedef struct Demo {

        int num;
        struct Demo *next;
}Dem;

struct Demo *head=NULL;
int sum=0;

void addnode() {

        Dem *newnode=(Dem*)malloc(sizeof(Dem));

        printf("Enter the number:\n");
        scanf("%d",&newnode->num);

        newnode->next=NULL;

        if(head==NULL) {
                head=newnode;
        }else{

                Dem *temp=head;
                while(temp->next != NULL) {
                        temp=temp->next;
                }
                temp->next=newnode;
	}
}

void Sum() {

	Dem *temp=head;

	while(temp!=NULL) {
		sum = sum + temp->num;
		temp = temp->next;
	
	}
}

void main() {

	addnode();
	addnode();
	addnode();
	addnode();
	Sum();
	printf("%d",sum);

}


