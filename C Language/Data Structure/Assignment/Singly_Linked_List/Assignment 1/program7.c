//print the maximum integer data from the above nodes. 


#include<stdio.h>
#include<stdlib.h>

typedef struct Demo {

        int num;
        struct Demo *next;
}Dem;

struct Demo *head=NULL;

void addnode() {

        Dem *newnode=(Dem*)malloc(sizeof(Dem));

        printf("Enter the number:\n");
        scanf("%d",&newnode->num);

	newnode->next=NULL;

	if(head==NULL) {
		head=newnode;
	}else{
		
		Dem *temp=head;
		while(temp->next=NULL) {
			temp=temp->next;
		}
		temp->next=newnode;
	}
}

int max() {

	Dem *temp=head;
	int num = temp->num;
	while(temp != NULL) {
		if(num < temp->num) {
			num=temp->num;
		}
		temp=temp->next;
	}
	return num;
}

void main() {

	addnode();
	addnode();
	addnode();
	int num=max();
	printf("\n Maximum number = %d",num);
}



