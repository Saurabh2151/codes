/*

  WAP for the linked list of malls consisting of its name,number of shops,&revenue;
  connect 3 malls in the linkedlist & print their data.

*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Malls {
	char Mname[20];
	int Nshop;
	float revenue;
	struct Malls *next;
}mal;

mal *head=NULL;

void addnode() {

	mal *newnode=(mal*)malloc(sizeof(mal));

	printf("enter Mname:\n");
	fgets(newnode->Mname,15,stdin);

	printf("enter Nshops:\n");
	scanf("%d",(&newnode->Nshop));

	printf("enter the revenue:\n");
	scanf("%f",(&newnode->revenue));

	getchar();

	newnode->next=NULL;

	if(head==NULL) {
		head=newnode;
	}else{
		mal *temp=head;
		while(temp->next!=NULL) {

			temp=temp->next;
		}
		temp->next=newnode;
	}
}

void printll() {

	mal *temp=head;
	while(temp!=NULL) {

		printf("%s",temp->Mname);
		printf("%d\n",temp->Nshop);
		printf("%f\n",temp->revenue);
		temp=temp->next;
	}
}

void main() {

	addnode();
	addnode();
	addnode();
	printll();
}




















