/*

6]  print the addition of first and last node data from the above code.

*/



#include<stdio.h>
#include<stdlib.h>

typedef struct Demo {

        int num;
        struct Demo *next;
}Dem;

struct Demo *head=NULL;
int sum=0;

void addnode() {

        Dem *newnode=(Dem*)malloc(sizeof(Dem));

        printf("Enter the number:\n");
        scanf("%d",&newnode->num);

	newnode->next=NULL;

	if(head==NULL) {
		head=newnode;
	}else{
		
		Dem *temp=head;
		while(temp->next=NULL) {
			temp=temp->next;
		}
		temp->next=newnode;
	}
}

void add() {

	Dem *temp=head;
	while(temp!=NULL) {
			
		sum=sum+temp->num;
		temp=temp->next;
	}
}

void main() {

	addnode();
	addnode();
	addnode();
	add();
	printf("%d\n",sum);
}
