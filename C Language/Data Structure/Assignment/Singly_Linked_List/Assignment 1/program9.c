

// WAP to check the prime number present in the data from the above codes.



#include<stdio.h>
#include<stdlib.h>

typedef struct Demo {

	int num;
	struct Demo *next;
}Dem;

Dem *head = NULL;

void addnode() {

	Dem *newnode=(Dem*)malloc(sizeof(Dem));

	printf("Enter the number:\n");
	scanf("%d",&newnode->num);

	newnode->next = NULL;

	if(head == NULL) {
		head=newnode;
	}else{
		Dem *temp = head;
		while(temp->next != NULL) {
			temp=temp->next;
		}
		temp->next=newnode;
	}
}

void prime() {

	Dem *temp = head;
	while(temp != NULL) {
		int flag=0;
		for(int i=2;i<=temp->num;i++) {

			if(temp->num % i==0) 
				flag++;
			}
			if(flag == 1) 
				printf("Prime number = %d\n",temp->num);
			if(temp->num==1) 
				printf("%d is not a prime not composite number\n",temp->num);
				temp=temp->next;
		}
		

}

void main() {

	addnode();
	addnode();
	addnode();
	prime();
}
