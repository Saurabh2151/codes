

#include<stdio.h>
#include<string.h>
typedef struct Employee {
	
	int emID;
	char ename[20];
	float sal;
	struct Employee *next;
}Emp;

void main() {

	Emp obj1,obj2,obj3;
	Emp *head = &obj1;

	obj1.emID=1;
	strcpy(obj1.ename,"Ashish");
	obj1.sal=50.45;
	obj1.next =&obj2;

	obj2.emID=2;
	strcpy(obj2.ename,"Saurabh");
	obj2.sal=60.00;
	obj2.next=&obj3;

	obj3.emID=3;
	strcpy(obj3.ename,"pankaj");
	obj3.sal=60.00;
	obj3.next=NULL;

	printf("%d\n",obj2.next->emID);
	printf("%s\n",obj2.next->ename);
	printf("%f\n",obj2.next->sal);

	printf("%d\n",obj1.next->emID);
        printf("%s\n",obj1.next->ename);
        printf("%f\n",obj1.next->sal);

	printf("%d\n",head->emID);
        printf("%s\n",head->ename);
        printf("%f\n",head->sal);
}
        


