
#include<stdio.h> 

int partition(int arr[],int start,int end) {

	int itr = start -1;
	int pivat = arr[end];

	for(int i=start; i< end;i++) {

		if(arr[i] < pivat) {

			itr++;
			int temp = arr[i];
			arr [i] = arr[itr];
			arr[itr] = temp;
		}
	}
	int temp = arr[itr+1];
	arr[itr+1] = arr[end];
	arr[end] = temp;

	return (itr+1);
}

void QuickSort(int arr[], int start,int end) {

	if(start < end ) {
		
		int pivat = partition(arr,start,end);
		QuickSort(arr,start,pivat-1);
		QuickSort(arr,pivat+1,end);
	}
}

void main() {

	int arr[] = {3,-2,-3,7,4,5};

	int size = sizeof(arr)/sizeof(int);
	
	for(int i=0;i<size;i++) {

		printf("%d ",arr[i]);
	}
	printf("\n");

	int start = 0;
	int end =size-1;
	
	QuickSort(arr,start,end);
	
	for(int i=0;i<size;i++) {

		printf("%d ",arr[i]);
	}
}

