

// Find middle Element in the given array.


#include<stdio.h>

void Sort(int arr[],int size) {

	for(int i=0;i<size-1;i++) {

		for(int j=0;j<size-1;j++) {

			if(arr[j] < arr[j+1]) {

				int temp = arr[j];
				arr[j] = arr[j+1];
				arr[j+1] = temp;
			}
		}
	}
} 

void main() {

	int arr[] = {-4,-2,1,3,4,5};
	
	int size = sizeof(arr)/sizeof(int);

	for(int i=0;i<size;i++) {

		printf("%d ",arr[i]);
	}
	printf("\n");

	printf("%d is a middle element\n",arr[(0 + size-1)/2]);
}

