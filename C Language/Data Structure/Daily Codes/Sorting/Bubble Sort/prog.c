

#include<stdio.h>

void bubbleSort(int *,int);

void main() {

	int size;

	printf("Enter the Array Size:\n");
	scanf("%d",&size);

	int arr[size];

	printf("Enter the Number:\n");

	for(int i=0;i<size;i++) {

		scanf("%d",&arr[i]);
	}

	for(int k=0;k<size;k++) {

		printf("%d\n",arr[k]);
	}

	bubbleSort(arr,sizeof(arr)/sizeof(int));
	
	for(int k=0;k<size;k++) {

		printf("%d\n",arr[k]);
	}

}

void bubbleSort(int arr[],int size) {

	for(int i=0;i< size-1;i++) {
		for(int j=0;j<size-i-1;j++) {

			if(arr[j] > arr[j+1]) {

				int temp = arr[j];
				arr[j] = arr[j+1];
				arr[j+1] = temp;
			}
		}
	}
}
