
#include<stdio.h> 

void BubbleSort(int *arr,int size) {

	int Count = 0;

	for(int j=0;j<size-Count-1;j++) {

		if(arr[j] > arr[j+1]) {

			int temp = arr[j];
			arr[j] = arr[j+1];
			arr[j+1] = temp;
		}
		if(j == size - Count - 2) {	
			j=-1;
			Count++;
		}
		if(Count == size-1) {
			
			break;
		}
	}
}

void main() {

	int arr[] = {8,3,6,-3,4,-7,9};
	
	int size = sizeof(arr)/sizeof(int);

	for(int i=0;i<size;i++) {

		printf("%d ",arr[i]);
	}
	printf("\n");

	BubbleSort(arr,size);

	for(int i=0;i<size;i++) {
		
		printf("%d ",arr[i]);
	}
}

