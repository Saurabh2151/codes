
#include<stdio.h>

void InsertionSort(int arr[],int size) {
	
	int val;
	for(int i=1;i<=size;i++) {

		val = arr[i];
		int j = i-1;

		for(  ;j>=0 && arr[j] > val;j--) {

			arr[j+1] = arr[j];
		}
		arr[j+1] = val;
	}
}

void main() {

	int arr[] = {3,5,2,1,-4,7,-9};

	int size = sizeof(arr)/sizeof(int);

	for(int i=0;i<size;i++) {

		printf("%d ",arr[i]);

	}

	printf("\n");

	InsertionSort(arr,size);

	for(int i=0;i<size;i++) {

		printf("%d ",arr[i]);
	}
}


