


  // Take Two Different Array Make Sort Them & Marge Into Third Array.
  


#include<stdio.h>

void Sort(int arr[],int size) {

	for(int i=0;i<size-1;i++) {

		for(int j=0;j<size-i-1;j++) {

			if( arr[j] > arr[j+1]) {

				int temp = arr[j];
				arr[j] = arr[j+1];
				arr[j+1] = temp;
			}
		}
	}
}

void MargeSort(int arr1[],int arr2[],int arr3[],int size1,int size2,int size3) {
	
	int itr1 = 0;
	int itr2 = 0;

	for(int i=0;i<size3;i++) {

		if(itr1<size1 && itr2<size2) {

			if(arr1[itr1] < arr2[itr2]) {

				arr3[i] = arr1[itr1];
				itr1++;
			}else{

				arr3[i] = arr2[itr2];
				itr2++;
			}

		}else{

			if(itr1 < size1) {

				arr3[i] = arr1[itr1];
				itr1++;

			}else{

				arr3[i] = arr2[itr2];
				itr2++;
			}
		}
	}
}

void main() {

	int arr1[] = {5,3,1,4,2};
	int arr2[] = {9,7,8,6};
	
	int size1 = sizeof(arr1)/sizeof(int);
	int size2 = sizeof(arr2)/sizeof(int);
	int size3 = size1 + size2;

	int arr3[size3];
	
	Sort(arr1,size1);
	Sort(arr2,size2);
	MargeSort(arr1,arr2,arr3,size1,size2,size3);

	for(int i=0;i<size3;i++) {

		printf("%d ",arr3[i]);
	}
}
