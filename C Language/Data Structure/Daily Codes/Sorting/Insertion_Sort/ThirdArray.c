

   // Two array Of Same Size Make Their Sum on Third Array




#include<stdio.h>

void Sum(int arr1[],int size,int arr2[],int arr3[]) {

	for(int i=0;i<size;i++) {

		arr3[i] = arr1[i] + arr2[i];
	}
}

void main() {

	int arr1[] = {2,3,4,5};
	int arr2[] = {6,7,8,9};

	int size = sizeof(arr1)/sizeof(int);
	int arr3[size];
	
	Sum(arr1,size,arr2,arr3);

	for(int i=0;i<size;i++) {

		printf("%d ",arr3[i]);
	}
}
