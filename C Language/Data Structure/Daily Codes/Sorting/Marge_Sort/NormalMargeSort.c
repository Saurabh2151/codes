

#include<stdio.h>

void MargeSort(int arr[],int start,int end) {

	if(start < end ) {

		int mid = (start + end)/2;

		MargeSort(arr,start,mid);
		MargeSort(arr,mid+1,end);

	}
}

void main() {

	int arr[] = {6,8,2,4,1,5,9,3,7};

	int size = sizeof(arr)/sizeof(int);
	int start=0;
	int end=size-1;
	MargeSort(arr,start,end);

	for(int i=0; i<size; i++) {

		printf("%d ",arr[i]);
	}
}

