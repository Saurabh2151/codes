
 // Sort given array im increasing order of order of number of dictict factor of each element .
 
 // * Least No. Should 1st.
 // * Highest No. Should Last.
 

#include<stdio.h>

int factor(int num) {

	int count = 0;

	for(int i=1; i<=num; i++) {

		if(num % i == 0) {

			count++;
		}
	}
	return count;
}

void IO(int arr[],int size) {

	for(int i=0; i<size-1; i++) {
		for(int j=0; j<size-i-1; j++) {

			if(factor(arr[j]) == factor(arr[j+1])) {

				if(arr[j] > arr[j+1]) {

					int temp = arr[j];
					arr[j] = arr[j+1];
					arr[j+1] = temp;

				}
			}
			if(factor(arr[j]) > factor(arr[j+1])) {

				int temp = arr[j];
				arr[j] = arr[j+1];
				arr[j+1] = temp;
			}
		}
	}
}

void main() {

	int arr[] = {6,8,9};

	int size = sizeof(arr)/sizeof(int);

	for(int i=0;i<size;i++) {

		printf("%d ",arr[i]);
	}
	printf("\n");

	IO(arr,size);

	for(int i=0;i<size;i++) {

		printf("%d ",arr[i]);
	}
}



