#include<stdio.h>

void SelectionSort(int arr[],int size) {

	for(int i=0; i<size-1; i++) {

		int minindex = i;

		for(int j=i+1; j<size; j++) {

			if(arr[minindex] > arr[j]) {

				minindex = j;

			}
		}
		int temp = arr[i];
		arr[i] = arr[minindex];
		arr[minindex] = temp;
	}
}

void main() {
	
	int arr[] = {4,5,3,8,6,-3,-2};

	int size = sizeof(arr)/sizeof(int);

	for(int i=0; i< size; i++) {

		printf("%d ",arr[i]);
	}
	printf("\n");

	SelectionSort(arr,size);

	for(int i=0; i< size; i++) {

		printf("%d ",arr[i]);
	}
}


