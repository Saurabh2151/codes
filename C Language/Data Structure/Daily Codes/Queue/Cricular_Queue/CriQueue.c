

//Circular queue using array

#include<stdio.h>

int size = 0;
int front = -1;
int rear = -1;

int flag = 0;

int Enqueue(int arr[]){

	if((rear == size -1) && (front == 0) || (rear == front -1)){
		return -1;
	}else{
		if(front == -1)
			front ++;
		else{
			if(rear == size -1)
				rear = -1;
		}
		rear++;
		printf("Enter data = ");
		scanf("%d",&arr[rear]);
		return 0;
	}
}

int Dequeue(int arr[]){

	if(front == -1){
		flag = 0;
		return -1;
	}else{
		flag = 1;
		int val = arr[front];

		if(front == rear){
			front = -1;
			rear = -1;
		}else{
			if(front == size - 1)
				front = -1;

			front ++;
		}
		return val;
	}
}

int Front(int arr[]){

	if(front == -1){
		flag = 0;
		return -1;
	}else{
		flag = 1;
		return arr[front];
	}
}

int PrintQueue(int *arr){

	if(rear == -1){
		flag = 0;
		return -1;
	}else{
		flag = 1;
	if(front <= rear){

		for(int i = front; i<= rear; i++){
			printf("%d  ",arr[i]);
		}
	}else{

		if(front > rear){
			for(int i = front; i<= size -1; i++){
				printf("%d  ",arr[i]);
			}
		}
		for(int i = 0; i<= rear; i++){
			printf("%d  ",arr[i]);
		}
		printf("\n");
	}
	}
}

void main(){

	printf("Enter size of queue = ");
	scanf("%d",&size);
	int arr[size];

	char Choice;

	do{

		printf("1. enqueue\n");
		printf("2. dequeue\n");
		printf("3. front\n");
		printf("4. Print queue\n");

		int ch;
		printf("Enter your choice = ");
		scanf("%d",&ch);

		switch(ch){

			case 1:
				{
				int ret = Enqueue(arr);

				if(ret == -1)
					printf("Queue Overflow\n");
				}
				break;

			case 2:
				{
				int ret = Dequeue(arr);

				if(flag == 0)
					printf("Queue Underflow\n");
				else
					printf("%d is dequeued\n",ret);
				}
				break;

			case 3:
				{
				int ret = Front(arr);

				if(flag == 0)
					printf("Queue Underflow\n");
				else
					printf("%d is front\n",ret);
				}
				break;

			case 4:
				{
				int ret = PrintQueue(arr);

				if(flag == 0)
					printf("Queue Underflow\n");
				}
				break;

			default :
				printf("Invalid choice\n");
		}

		getchar();
		printf("\nDo you want to continue? = ");
		scanf("%c",&Choice);
	}while(Choice == 'Y' || Choice == 'y');
}
