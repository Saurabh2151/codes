// Sorting Linked List By priority.
// while adding Node in desending order.

#include<stdio.h>
#include<stdlib.h>
typedef struct Node {

	int data;
	int Priority;
	struct Node *next;
}node;

node *head = NULL;

int flag = 0;

node* createNode() {
	
	node *newnode=(node*)malloc(sizeof(node));

	printf("Enter the Data:\n");
	scanf("%d",&newnode->data);

	do {

		printf("Enter the Priority:\n");
		scanf("%d",&newnode->Priority);

	}while(newnode->Priority < 0 || newnode->Priority > 5);
	
	newnode->next = NULL;
	return newnode;

}

void addFirst() {
	
	node *newnode = createNode();

	if(head == NULL) {

		head = newnode;

	}else {

		newnode->next = head;
		head = newnode;
	}
}

int AddPriority() {
	
	if(head == NULL) {

		addFirst();
		return 0;

	}else{
		node *newnode = createNode();
		
		if(newnode->Priority > head->Priority) {
	
			newnode->next = head ;
			head = newnode;
			return 0;
		}

		node *temp = head;
			
		while(temp->next != NULL) {
					
				if(temp->Priority == temp->next->Priority) {

					temp = temp->next;
				}else if(newnode->Priority >= temp->Priority && newnode->Priority >= temp->next->Priority ) {
					
					newnode->next = temp->next;
					temp->next = newnode;
					return 0;
				}
				temp = temp->next;
		}
		temp->next = newnode;
		return 0;
	}
}

int dequeue() {

	if(head == NULL) {
		flag =0;
		return 0;

	}else{

		flag=1;
		node *temp = head;
		head = head->next;
		int val = temp->data;
		free(temp);
		return val;
	}
}

int printPriority() {

	if(head == NULL) {

		flag =0;
		return-1;
	}else{

		flag=1;

		node *temp = head;

		while(temp->next != NULL) {

			printf("|%d|-->",temp->Priority);
			temp=temp->next;
		}
		printf("|%d|\n",temp->Priority);
	}
}

void main() {

	char Choice;

	do {

		printf("1.AddPriority\n");
		printf("2.dequeue\n");
		printf("3.PrintPriority\n");

		int Num;
		printf("Enter the Choice\n");
		scanf("%d",&Num);

		switch(Num) {

			case 1 :

				{
				int nodes;
				printf("Enter the no of Nodes\n");
				scanf("%d",&nodes);

				for(int i=1;i<=nodes;i++) {

					AddPriority();
				}
				}
				break;

			case 2 :

				{
				int ret = dequeue();

				if(flag == 0) {
					printf("List is Empty\n");
				}else{
					printf("%d Dequeue\n",ret);

				}
				}
				break;

			case 3 :
				
				{

				printPriority();

				if(flag == 0) 

					printf("Link List is Empty\n");
				}
				break;
			
			default :

				printf("wrong Choice\n");
		}

		getchar();
		printf("\nDo you want to continue:\n");
		scanf("%c",&Choice);

	}while(Choice == 'y'  || Choice == 'Y');
}




