
//Singly__Circular__LL



#include<stdio.h>
#include<stdlib.h>

typedef struct Node {

	int data;
	struct Node *next;
}node;

node *head = NULL;

node* createNode() {

	node *newnode=(node*)malloc(sizeof(node));

	printf("\nEnter the Data\n");
	scanf("%d",&newnode->data);

	newnode->next=head;

	return newnode;
}

void addnode() {

	node *newnode=createNode();

	if(head == NULL) {

		head = newnode;
		newnode->next = head;
		
	}else{

		node*temp = head;

		while(temp->next != head) {

			temp=temp->next;
		}
		temp->next=newnode;
		newnode->next = head;
	}

}

void addfirst() {

	node *newnode = createNode();

	if(head == NULL) {
		head = newnode;
		newnode->next = head;
	}else{
		
		node *temp = head;

		while(temp->next != head) {

			temp=temp->next;
		}
		temp->next = newnode;
		newnode->next=head;
		head = newnode;
	}
}

void addlast() {

	addnode();

}

int countNode() {

        if(head == NULL) {
                printf("Count is = 0\n");
        }else{
                 node *temp = head;

                 int count=0;
                 while(temp->next != head) {

                        temp=temp->next;
                        count++;
                 }
                 count++;
                 printf("\ncount is = %d\n",count);
                return count;
        }
}

int addAtPos(int pos) {
	
	int count = countNode();

	if(pos <= 0 || pos >= count + 1) {
		printf("Wrong choice\n");
		return -1;

	}else if(pos == count + 1) {
		addlast();

	}else if(pos == 1) {
		addfirst();
	}else{
		
		node *newnode = createNode();
		node *temp = head;

		while(pos - 2) {
				
			temp=temp->next;
			pos--;
		}
		newnode->next = temp->next;
		temp->next = newnode;
	}
	return 0;
}

int deletefirst() {
	
	if(head == NULL) {
		printf("Empty string plzz Addnode\n");
	}else if(head->next != head) {

		free(head);
		head = NULL;
		return -1;
			
	}else{
		node *temp = head;
	
		while(temp->next != head) {

			temp=temp->next;
		}
		head = head->next;
		free(temp->next);
		temp->next=head;
	}
}

int deleteLast() {
	
	if(head == NULL) {
		printf("Empty String plzz Addnode\n");

	}else if(head->next != head) {

		free(head);
		head = NULL;
		return -1;

	}else{
		node *temp = head;

		while(temp->next->next != head) {

			temp=temp->next;
		}
		free(temp->next);
		temp->next = head;
	}
}

int deleteAtPos(int pos1) {

	if(head == NULL) {
		printf("Empty String Plzz Addnode\n");
	}else{

		int count1 = countNode();
	
		if(pos1 <=0 || pos1 > count1) {
			printf("Wrong Choice\n");
			return -1;

		}else if(pos1 == 1) {
			deletefirst();

		}else if(pos1 == count1) {
			deleteLast();

		}else{

			node *temp=head;

			while(pos1 - 2) {

				temp=temp->next;
				pos1--;
			}
			node *temp1 = temp;
			temp1=temp->next;
			temp->next = temp->next->next;
			free(temp1);
		}
		return 0;
	}
}

void printll() {
	
	if(head == NULL) {
		printf("Empty String Boss");
	}	else{	
		node *temp = head;

		while(temp->next != head) {

			printf("|%d|-->",temp->data);
			temp=temp->next;
		}
		printf("|%d|\n",temp->data);
	}
}

void main() {

	char Choice;

	do {

		printf("1.addnode\n");
		printf("2.countNode\n");
		printf("3.addfirst\n");
		printf("4.addLast\n");
		printf("5.addAtPos\n");
		printf("6.deletefirst\n");
		printf("7.deleteLast\n");
		printf("8.deleteAtPos\n");
		printf("9.printll\n");

		int Num;
		printf("Enter the Choice\n");
		scanf("%d",&Num);

		switch(Num) {

			case 1 :
				
				{
				int nodes;
			        printf("Enter the no of nodes\n");
			        scanf("%d",&nodes);

        		        for(int i=1;i<=nodes;i++) {

                        		addnode();
				}
				}
				break;

			case 2 :

				countNode();
				break;

			case 3 :

				addfirst();
				break;

			case 4 :
				 
				addlast();
				break;

			case 5 :
				
				{
				int pos;
				printf("Enter the Position\n");
				scanf("%d",&pos);

				addAtPos(pos);
				}
				break;

			case 6 :

				deletefirst();
				break;

			case 7 :

				deleteLast();
				break;

			case 8 :
				
				{
				int pos1;
				printf("Enter the Position 1\n");
				scanf("%d",&pos1);

				deleteAtPos(pos1);
				}
				break;

			case 9 : 

				printll();
				break;

			default :

				printf("wrong Choice\n");
		}

		getchar();
		printf("\nDo you want to continue:\n");
		scanf("%c",&Choice);

	}while(Choice == 'y'  || Choice == 'Y');
}	

