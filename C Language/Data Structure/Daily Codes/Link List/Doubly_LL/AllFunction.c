
#include<stdio.h>
#include<stdlib.h>

typedef struct Node {
	
	struct Node *prev;
	int data;
	struct Node *next;
}node;
 
node *head = NULL;

node* createNode() {

	node *newnode=(node*)malloc(sizeof(node));

	printf("\nEnter the Data:\n");
	scanf("%d",&newnode->data);

	newnode->next=NULL;

	return newnode;
}
void addnode() {

	node *newnode = createNode();

	if(head == NULL) {
		head = newnode;
	}else{
		node *temp = head;
		while(temp->next != NULL) {

			temp=temp->next;
		}
		temp->next = newnode;
		newnode->prev = temp;
	}	
}

void addfirst() {

	node *newnode = createNode();

	if(head == NULL) {

		head = newnode;
	}else{
		node* temp=head;

		newnode->next = temp;
		head->prev = newnode;
		head = newnode;
	}
}

void addLast() {

	addnode();
}

int CountNode() {

	node *temp = head;

	int count = 0;
	while(temp->next != NULL) {

		temp = temp->next;
		count++;
	}
	count++;
	printf("\nCount is = %d",count);
	return count;
}

int addAtpos(int pos) {
	
	int count = CountNode();
	if(pos <=0 || pos >= count+2) {

		printf("Wrong choice:\n");
		return -1;
	}
	else if(pos == count+1) {

		addLast();
	}
	else if(pos == 1) {

		addfirst();
	}
	else{

		node *newnode = createNode();
		node *temp = head;

		while(pos-2) {	
			temp = temp->next;
			pos--;
		}
		newnode->next = temp->next;
		newnode->prev = temp;
		temp->next->prev = newnode;
		temp->next = newnode;
	}
	return 0;
}

void deletefirst() {

        node *temp = head;

        while(temp->next != NULL) {

                temp=temp->next;
        }
        head = head->next;
        free(head->prev);
        head->prev = NULL;

}

void deleteLast() {
		
	node *temp = head;

	while(temp->next->next != NULL) {

		temp=temp->next;
	}
	free(temp->next);
	temp->next = NULL;
}

int deleteAtPos(int pos1) {
		
	int count1 = CountNode();

	if(pos1 <= 0 || pos1 > count1 ) {
		printf("Wrong Choice\n");
		return -1;
	}else if(pos1 == 1) {

		deletefirst();
	}else if(pos1 == count1) {

		deleteLast();
	}else {

		node *temp = head;

		while(pos1 - 2) {

			temp=temp->next;
			pos1--;
		}
		temp->next = temp->next->next;
		free(temp->next->prev);
		temp->next->prev = temp;
	}
	return 0;
}	

void printll() {

	if(head == NULL) {

		printf("List is Empty\n");
	}else{

		node *temp = head;
		while(temp->next != NULL) {

			printf("|%d|-->",temp->data);
			temp=temp->next;
		}
		printf("|%d|\n",temp->data);
	}
}

void main() {

	char Choice;

	do {

		printf("1.addnode\n");
		printf("2.CountNode\n");
		printf("3.addfirst\n");
		printf("4.addLast\n");
		printf("5.addAtPos\n");
		printf("6.deletefirst\n");
		printf("7.deleteLast\n");
		printf("8.deleteAtPos\n");
		printf("9.printll\n");
		
		int Num;
                printf("Enter your Choice\n");
                scanf("%d",&Num);

		switch(Num) {

			case 1 :
				
				{
				int Nodes;
				printf("Enter the nof nodes:\n");
				scanf("%d",&Nodes);

					for(int i=1;i<=Nodes;i++) {

						addnode();
					}
				}
				break;

			case 2 :

				CountNode();
				break;

			case 3 :

				addfirst();
				break;

			case 4 :

				addLast();
				break;

			case 5 :
				
				{
				int pos;
				printf("Enter the Position Number\n");
				scanf("%d",&pos);

				addAtpos(pos);
				}
				break;

			case 6 :

				deletefirst();
				break;

			case 7 :

				deleteLast();
				break;

			case 8 :
				
				{
				int pos1;
				printf("Enter the delete Position Number\n");
				scanf("%d",&pos1);

				deleteAtPos(pos1);
				}
				break;

			case 9 :

				printll();
				break;

			default :

				printf("Wrong Choice\n");
		}
		
		getchar();
		printf("Do You Want To Continue\n");
		scanf("%c",&Choice);

		}while(Choice == 'Y' || Choice == 'y');
}
