

#include<stdio.h>
#include<stdlib.h>

typedef struct Node {
	
	struct Node *prev;
	int data;
	struct Node *next;
}node;

node *head = NULL;

node* createNode() {

	node *newnode=(node*)malloc(sizeof(node));

	printf("\nEnter the Data\n");
	scanf("%d",&newnode->data);

	newnode->next=head;

	return newnode;
}

void addnode() {

	node *newnode=createNode();

	if(head == NULL) {

		head = newnode;
		newnode->next = head;
		newnode->prev = head;
	}else{

		head->prev->next = newnode;
		newnode->prev = head->prev;
		newnode->next = head;
		head->prev = newnode;
	}
}

void Addfirst() {

	node *newnode = createNode();

	if(head == NULL) {
		head = newnode;
		head->next = head;
		head->prev = head;

	}else{

		newnode->next = head;
		newnode->prev = head->prev;
		head->prev = newnode;
		head = newnode;
		newnode->prev->next = head;
	}
}

void AddLast() {
	
	addnode();
}

int countNode() {
	
	int count = 0;

	if(head == NULL) {
		
		printf("Count is = %d ",count);
		return -1;
	}else{
		node *temp = head;

		while(temp->next != head) {
	
			count++;
			temp = temp->next;
		}
		count++;
		printf("count is = %d",count);
	}
}

int addAtPos(int pos) {

	if(head == NULL) {

		printf("String is Empty Boss\n");

	}else{
		node *newnode = createNode();

		int count1 = countNode();

		if(pos <= 0 || pos > count1 + 1) {

			printf("Wrong Choice Boss\n");
			return -1;

		}else if(pos == 1) {

			Addfirst();

		}else if(pos == count1+1) {

			AddLast();

		}else{

			node *temp = head;

			while(pos - 2) {

				temp = temp->next;
				pos--;
			}
			newnode->next = temp->next;
			temp->next->prev = newnode;
			temp->next = newnode;
			newnode->prev = temp;
		}
		return 0;
	}

}

int deletefirst() {

	if(head == head) {

		free(head);
		head = NULL;

	}else{
		
		head->next->prev = head->prev;
		head =head->next;
		free(head->prev->next);
		head->prev->next = head;
	}
	return 0;
}

int deleteLast() {
	
	if(head == head) {

		free(head);
		head = NULL;

	}else{
	
		head->prev = head->prev->prev;
		free(head->prev->next);
		head->prev->next = head;
	}
	return 0;
}

int deleteAtPos(int pos1) {

	if(head == NULL) {

		printf("String is Empty Boss\n");

	}else{
		
		int count = countNode();

		if(pos1 <= 0 || pos1 > count) {

			printf("Wrong Choice\n");
			return -1;
		
		}else if(pos1 == 1) {

			deletefirst();

		}else if(pos1 == count) {

			deleteLast();

		}else{

			node *temp = head;

			while(pos1 - 2) {

				temp = temp->next;
				pos1--;
			}
			temp->next = temp->next->next;
			free(temp->next->prev);
			temp->next->prev = temp;
		}
		return 0;
	}

}

void printll() {
	
	if(head == NULL) {
		printf("Empty String Boss");

	}else{	
		node *temp = head;

		while(temp->next != head) {

			printf("|%d|-->",temp->data);
			temp=temp->next;
		}
		printf("|%d|\n",temp->data);
	}
}

void main() {
	
	char Choice;

	do {

		printf("1.Addnode\n");
		printf("2.countNode\n");
		printf("3.Addfirst\n");
		printf("4.AddLast\n");
		printf("5.addAtPos\n");
		printf("6.deletefirst\n");
		printf("7.deleteLast\n");	
		printf("8.deleteAtPos\n");
		printf("9.printll\n");
	
		int Num;
                printf("Enter your Choice\n");
                scanf("%d",&Num);

		switch(Num) {

			case 1 :
				
				{
				int Nodes;
				printf("Enter the nof nodes:\n");
				scanf("%d",&Nodes);

					for(int i=1;i<=Nodes;i++) {

						addnode();
					}
				}
				break;

			case 2 :

				countNode();
				break;

			case 3 :

				Addfirst();
				break;

			case 4 :

				AddLast();
				break;

			case 5 :
				
				{
				int pos;
				printf("Enter the Position Number\n");
				scanf("%d",&pos);

				addAtPos(pos);
				}
				break;

			case 6 :

				deletefirst();
				break;

			case 7 :

				deleteLast();
				break;

			case 8 :
				
				{
				int pos1;
				printf("Enter the delete Position Number\n");
				scanf("%d",&pos1);

				deleteAtPos(pos1);
				}
				break;

			case 9 :

				printll();
				break;

			default :

				printf("Wrong Choice\n");
		}
		
		getchar();
		printf("\nDo You Want To Continue\n");
		scanf("%c",&Choice);

	}while(Choice == 'y' || Choice == 'Y');
}

