
#include<stdio.h>

int Interpolation(int *arr,int size,int key) {

	int start = 0;
	int End = size-1;

	int Formula = start + key - arr[start]/arr[End] - arr[start] * (End - start);

	return Formula;

}

void main () {

	int arr[] = {2,4,6,8,10,12};
	
	int ele;
	printf("Enter the Element:\n");
	scanf("%d",&ele);

	int ret = Interpolation(arr,sizeof(arr)/sizeof(int),ele);

	if(ret != -1) {

		printf("Array Element Are Found %d index\n",ret);
	}else{
		printf("Array index Are not Found\n");
	}
}

